import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sto/InputWidget.dart';

import 'ResultWidget.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: WholeOutput(),
    );
  }
}

class WholeOutput extends StatefulWidget {
  @override
  _WholeOutputState createState() => _WholeOutputState();
}

class _WholeOutputState extends State<WholeOutput> {
  List<String> listOfSolutions = <String>[];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Solution finder'),
        actions: [
          RaisedButton(
            color: Colors.blue,
            onPressed: () => exit(0),
            child: Text("Exit"),
          )
        ],
      ),
      body: Column(
        children: [
          Container(
            child: InputWidget(updateInputInParent: getSolutions),
            height: 110,
            padding: EdgeInsets.fromLTRB(20, 10, 10, 10),
            margin: EdgeInsets.all(0),
          ),
          Expanded(
            child: Container(
              child: ResultWidget(solutions: listOfSolutions),
            ),
          )
        ],
      ),
    );
  }

  void getSolutions(List<String> solutions) {
    setState(() {
      listOfSolutions = solutions;
    });
  }
}
