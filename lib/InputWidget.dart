import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'Logic/Finder.dart';

class InputWidget extends StatefulWidget {
  @override
  _InputWidgetState createState() => _InputWidgetState();

  final ValueChanged<List<String>> updateInputInParent;

  const InputWidget({Key key, this.updateInputInParent});
}

class _InputWidgetState extends State<InputWidget> {
  final numberController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      body: Row(
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new TextField(
                  controller: numberController,
                  decoration:
                      new InputDecoration(labelText: "Enter your number"),
                  style: TextStyle(fontSize: 18),
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.singleLineFormatter
                  ], // Only numbers can be entered
                ),
              ],
            ),
          ),
          Container(
            width: 150,
            height: 100,
            padding: EdgeInsets.all(15),
            child: RaisedButton(
              color: Colors.lightBlue,
              child: Text('Show solutions'),
              textColor: Colors.black,
              onPressed: _proceedClick,
              padding: const EdgeInsets.all(5),
            ),
          )
        ],
      ),
    );
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    numberController.dispose();
    super.dispose();
  }

  void _proceedClick() {
    int value = int.tryParse(numberController.text);
    if (value == null) return;

    var finder = Finder.setInputNumber(inputNumber: value.toDouble());
    var result = finder.findSolutions();
    widget.updateInputInParent(result);
  }
}
