import 'package:flutter/material.dart';

class ResultWidget extends StatefulWidget {
  final List<String> solutions;

  ResultWidget({this.solutions});

  @override
  _ResultWidgetState createState() => _ResultWidgetState();
}

class _ResultWidgetState extends State<ResultWidget> {
  get solutionCount => widget.solutions.length;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          title: Center(
              child: Text(
            'Solutions: $solutionCount',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          )),
        ),
        Expanded(
          child: ListView.separated(
              padding: EdgeInsets.fromLTRB(20, 0, 10, 20),
              itemBuilder: (BuildContext _context, int i) {
                return Text(
                  widget.solutions[i],
                  style: TextStyle(fontSize: 16),
                );
              },
              separatorBuilder: (context, index) => const Divider(),
              itemCount: widget.solutions.length),
        ),
      ],
    );
  }
}
