import 'package:sto/Logic/Digit.dart';
import 'package:sto/Logic/Operation.dart';

class Finder {
  final numbers = <double>[1, 2, 3, 4, 5, 6, 7, 8, 9];
  var operations = <Operation>[];
  final double inputNumber;

  Finder.setInputNumber({this.inputNumber});

  List<String> findSolutions() {
    var operationsToTest = <Operation>[];

    for (int i = 0; i < numbers.length - 1; i++)
      operationsToTest.add(Operation(
          firstNumber: numbers[i],
          secondNumber: numbers[i + 1],
          digit: Digit.Add));

    var listOfAnswers = <String>[];
    var counter = 0;

    while (true) {
      if (operationsToTest.every((op) => op.digit == Digit.Union)) {
        break;
      }
      operations = <Operation>[...operationsToTest];
      var result = _calculate();
      _refreshNumbers(operationsToTest);
      if (result != null && result == inputNumber) {
        counter++;

        listOfAnswers.add(_generateAnswerString(operationsToTest, counter));
      }
      _goToNestStep(operationsToTest);
    }

    if (listOfAnswers.isEmpty) return <String>[];

    return listOfAnswers;
  }

  String _generateAnswerString(List<Operation> operations, int counter) {
    String operationsString = '';
    for (var op in operations) {
      var firstNum = op.firstNumber.toInt();
      var digit;

      switch (op.digit) {
        case Digit.Add:
          digit = '+';
          break;

        case Digit.Substract:
          digit = '-';
          break;

        case Digit.Multiply:
          digit = '\u00B7';
          break;

        case Digit.Divide:
          digit = ':';
          break;

        case Digit.Union:
          digit = '';
          break;
      }

      operationsString += '$firstNum$digit';
    }
    operationsString += '9';

    int inputInt = inputNumber.toInt();
    return '$counter) $operationsString = $inputInt';
  }

  double _calculate() {
    _dealWithUnions();
    if (!_dealWithPriorityOperations()) return null;

    double result;
    while (operations.length != 0) {
      result = operations[0].makeOperation();
      if (operations.length > 1) {
        operations[1].firstNumber = result;
      }
      operations.removeAt(0);
    }
    return result;
  }

  void _dealWithUnions() {
    while (true) {
      if (!operations.any((x) => x.digit == Digit.Union)) break;

      var priorityOperation =
          operations.lastWhere((x) => x.digit == Digit.Union);
      int indexOfOperation = operations.indexOf(priorityOperation);
      double result = priorityOperation.makeOperation();
      if (indexOfOperation != 0) {
        operations[indexOfOperation - 1].secondNumber = result;
      }
      if (indexOfOperation != operations.length - 1) {
        operations[indexOfOperation + 1].firstNumber = result;
      }
      operations.removeAt(indexOfOperation);
    }
  }

  bool _dealWithPriorityOperations() {
    bool flag;
    try {
      while (true) {
        if ((!operations.any((x) =>
                x.digit == Digit.Multiply ? true : x.digit == Digit.Divide)) ||
            operations.length == 1) break;

        var priorityOperation = operations.firstWhere(
            (x) => x.digit == Digit.Multiply ? true : x.digit == Digit.Divide);

        var indexOfOperation = operations.indexOf(priorityOperation);
        var result = priorityOperation.makeOperation();

        if (indexOfOperation != 0) {
          operations[indexOfOperation - 1].secondNumber = result;
        }
        if (indexOfOperation != operations.length - 1) {
          operations[indexOfOperation + 1].firstNumber = result;
        }
        operations.removeAt(indexOfOperation);
      }
      flag = true;
    } on Exception {
      flag = false;
    }

    return flag;
  }

  void _goToNestStep(List<Operation> _operations) {
    for (int i = 7; i >= 0; i--) {
      if (i > 0 &&
          _operations[i].digit != Digit.Union &&
          _allPreviousUnion(_operations, i)) {
        Operation item = _operations[i];

        item.digit = Digit.values[item.digit.index + 1];
        for (int j = 0; j < i; j++) {
          _operations[j].digit = Digit.Add;
        }
        return;
      }
      if (i == 0) {
        Operation operationToDo = _operations[i];
        operationToDo.digit = Digit.values[operationToDo.digit.index + 1];
      }
    }
  }

  bool _allPreviousUnion(List<Operation> _operations, int index) {
    bool result = true;
    for (int i = 0; i < index; i++) {
      result = (!result ? false : _operations[i].digit == Digit.Union);
    }
    return result;
  }

  void _refreshNumbers(List<Operation> _opertions) {
    for (int i = 0; i < 8; i++) {
      _opertions[i].firstNumber = numbers[i];
      _opertions[i].secondNumber = numbers[i + 1];
    }
  }
}
