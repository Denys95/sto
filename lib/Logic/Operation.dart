import 'package:sto/Logic/Digit.dart';

class Operation {
  double firstNumber;
  double secondNumber;

  Digit digit;

  Operation({this.firstNumber, this.secondNumber, this.digit});

  double makeOperation() {
    switch (digit) {
      case Digit.Add:
        return firstNumber + secondNumber;

      case Digit.Substract:
        return firstNumber - secondNumber;

      case Digit.Multiply:
        return firstNumber * secondNumber;

      case Digit.Divide:
        return firstNumber / secondNumber;

      case Digit.Union:
        {
          String firstStringNumber = firstNumber.toInt().toString();
          String secondStringNumber = secondNumber.toInt().toString();

          String finalStringNumber = firstStringNumber + secondStringNumber;

          return double.parse(finalStringNumber);
        }
    }
    return 0;
  }
}
